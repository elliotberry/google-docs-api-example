const { google } = require("googleapis");
const token = require("./token.json");
const credentials = require("./credentials.json");

function authorize() {
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id,
    client_secret,
    redirect_uris[0]
  );
  oAuth2Client.setCredentials(token);
  return oAuth2Client;
}

async function update(id) {
    const auth = await authorize();
    const docs = google.docs({
      version: "v1",
      auth
    });
    await docs.documents.batchUpdate({
      auth,
      documentId: id,
      requestBody: {
        requests: [
          {
            insertText: {
              location: {
                index: 1
              },
              text: "hello!\n"
            }
          }
        ]
      }
    });
}
async function get(id) {
    const auth = await authorize();
    const docs = google.docs({
      version: "v1",
      auth
    });
   let r = await docs.documents.get({
      auth,
      documentId: id,
    });
    let paras = [];
    r.data.body.content.forEach(function(item) {
        if (item.paragraph) {
           paras.push(item.paragraph.elements[0].textRun.content);
        }
        
    });
    return paras.join('');
}
get("138GJ4PEf_I1T4ZJiqsj9LJNTTTNFbEnKSmUdhm1NSzw");